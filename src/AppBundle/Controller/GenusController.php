<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GenusController
 * @package AppBundle\Controller
 */
class GenusController extends Controller
{
    /**
     * @Route("/genus/new")
     */
    public function newAction()
    {
        $genus = new Genus();
        $genus->setName("Octopus" . rand(1, 100));
        $genus->setSpeciesCount(rand(100, 1000));
        $genus->setSubFamily("Octopodiane");

        $genus_note=new GenusNote();
        $genus_note->setUsername("anyuser");
        $genus_note->setUserAvatarFilename("ryan.jpeg");
        $genus_note->setNote("This is a note of genus.");
        $genus_note->setCreatedAt(new \DateTime("-1 month"));
        $genus_note->setGenus($genus);

        $em = $this->getDoctrine()->getManager();
        $em->persist($genus);
        $em->persist($genus_note);
        $em->flush();

        return new Response("Genus Created!");
    }

    /**
     * @Route("/genus")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $genuses = $em->getRepository("AppBundle:Genus")
            ->findAllPublishedOrderRecentlyActive();

        return $this->render("genus/list.html.twig", [
            'genuses' => $genuses,
        ]);
    }

    /**
     * @Route("/genus/{genusName}" , name="genus_show")
     */
    public function showAction($genusName)
    {
        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository("AppBundle:Genus")->findOneBy(["name" => $genusName]);

        if (!$genus) {
            throw $this->createNotFoundException("Genus Not Found");
        }
        /*$funFacts = "Octopuses can change the color of their body in just *three-tenths* of a second!";
        $cache = $this->get('doctrine_cache.providers.my_markdown_cache');
        $key = md5($funFacts);

        if ($cache->contains($key)) {
            $funFacts = $cache->fetch($key);
        } else {
            $funFacts = $this->get('markdown.parser')->transform($funFacts);
            $cache->save($key, $funFacts);
        }

        $notes = [
            'Octopus asked me a riddle',
            'This is the Second line',
            'And yeah this is the third line'
        ];*/

        $recent_notes=$em->getRepository("AppBundle:GenusNote")
            ->findAllRecentNotesForGenus($genus);

        return $this->render('genus/show.html.twig', array(
            'genus' => $genus,
            'recent_notes' => count($recent_notes)
        ));
    }

    /**
     * @Route("/genus/{name}/notes", name="genus_show_notes")
     * @Method("GET")
     */
    public function getNotesAction(Genus $genus)
    {
        $notes=[];
        foreach ($genus->getNotes() as $note) {
            $notes[]=
            [
                'id' => $note->getId(),
                'username' => $note->getUsername(),
                'avatarUri' => '/images/'.$note->getUserAvatarFilename(),
                'note' => $note->getNote(),
                'date' => $note->getCreatedAt()->format('M d, Y')
            ];
        }
        $data = [
            'status' => true,
            'notes' => $notes
        ];

        return new JsonResponse($data);
    }
}