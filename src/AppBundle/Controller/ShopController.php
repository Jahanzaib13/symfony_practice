<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ShopController
 * @package AppBundle\Controller
 */
class ShopController extends Controller
{
    /**
     * @Route("/getUserShops/user_id/{user_id}", name="get_user_shops")
     */
    public function userShopsAction($user_id){
        $em=$this->getDoctrine()->getManager();
        $shops=$em->getRepository('AppBundle:Shop')
            ->findBy(['user'=> $user_id]);

        return $this->render("shop/showUserShopDetails.html.twig",[
            "shops" => $shops
        ]);
    }

    /**
     * @Route("/saveShopByUser/user_id/{user_id}/shop_id/{shopId}/shop_password/{shopPassword}")
     */
    public function saveUserShopAction($user_id, $shopId, $shopPassword){

        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('AppBundle:User')
            ->find($user_id);

        $shop= new Shop();
        $shop->setShopId($shopId);
        $shop->setUser($user);
        $shop->setShopPassword($shopPassword);

        $em= $this->getDoctrine()->getManager();
        $em->persist($shop);
        $em->flush();

        return new Response("Shop Created!");
    }
}