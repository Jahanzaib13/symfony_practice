<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use function PHPSTORM_META\type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    public function saveUserDetailsAction($username,$firstname,$lastname,$email,$type){
        $user=new User();
        $user->setUsername($username);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setType($type);

        $em=$this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return new Response("User Created!");
    }

    /**
     * @Route("showUserDetails/{key}")
     */
    public function showUserDetails($key){
        $em=$this->getDoctrine()->getManager();
        $users=$em->getRepository("AppBundle:User")
            ->findAllUsersByKey($key);

        foreach ($users as $user){
            $user->is_valid = "No";
            if ($user->getType() == $this->getParameter("user_type")){
                $user->is_valid = "Yes";
            }
        }
        $template=$this->get('twig')->render(":user:showUserDetails.html.twig", [
            'users' => $users
        ]);

        echo $template;
        return new Response(null);

        /*return $this->render("user/showUserDetails.html.twig",[
            "users" => $users
        ]);*/
    }
}