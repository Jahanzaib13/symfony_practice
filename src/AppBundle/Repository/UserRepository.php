<?php

namespace AppBundle\Repository;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package AppBundle\Repository
 */
class UserRepository Extends EntityRepository
{
    /**
     * @param $key
     * @return User[]
     */
    public function findAllUsersByKey($key){
        return $this->createQueryBuilder("user")
            ->where("user.id = :key")
            ->orWhere("user.username = :key")
            ->orWhere("user.firstname = :key")
            ->orWhere("user.lastname = :key")
            ->orWhere("user.email = :key")
            ->orWhere("user.type = :key")
            ->setParameter('key', $key)
            ->getQuery()
            ->execute();
    }
}