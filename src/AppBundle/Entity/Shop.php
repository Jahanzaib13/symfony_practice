<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop")
 */
class Shop
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $shopId;

    /**
     * @ORM\Column(type="string")
     */
    private $shopPassword;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="shops")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Supplier", mappedBy="shops")
     * @ORM\JoinColumn(nullable=false)
     */
    private $suppliers;

    public function __construct()
    {
        $this->suppliers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getShopId():int
    {
        return $this->shopId;
    }

    /**
     * @param $shopId
     */
    public function setShopId(int $shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @return mixed
     */
    public function getShopPassword():string
    {
        return $this->shopPassword;
    }

    /**
     * @param $shopPassword
     */
    public function setShopPassword(string $shopPassword)
    {
        $this->shopPassword = $shopPassword;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

}